package HW004;

import java.util.Arrays;

public class Pet {
    private String species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    public Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    public String getSpecies() {
        return this.species;
    }

    public String getNickname() {
        return this.nickname;
    }

    public int getAge() {
        return this.age;
    }

    public int getTrickLevel() {
        return this.trickLevel;
    }

    public void eat() {
        System.out.println("Я кушаю!");
    }

    public void respond() {
        System.out.printf("Привет, хозяин. Я - %s. Я соскучился!", this.nickname);
    }

    public void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }
//    dog{nickname='Rock', age=5, trickLevel=75, habits=[eat, drink, sleep]}
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("");
        sb
                .append(this.species)
                .append("{nickname = ").append(this.nickname)
                .append(", age = ").append(this.age)
                .append(", trickLevel = ").append(this.trickLevel)
                .append(", trickLevel = ").append(Arrays.toString(this.habits));
        return sb.toString();
    }
}