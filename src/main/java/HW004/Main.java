package HW004;

public class Main {
    public static void main(String[] args) {
        Human mother = new Human("Mother Name", "Mother Surname", 1970);
        Human father = new Human("Father Name", "Father Surname", 1965);
        Pet pet = new Pet("Dog", "Rocky", 5, 100, new String[]{"eat", "drink", "sleep"});
//        this.species = species;
//        this.nickname = nickname;
//        this.age = age;
//        this.trickLevel = trickLevel;
//        this.habits = habits;
        Human h1 = new Human();

        Human h2 = new Human("H2 name", "H2 surname", 1990);

        Human h3 = new Human("H2 name", "H2 surname", 1990, 200, pet, mother, father, new String[][]{});

        System.out.println(h2.getName());

//        System.out.println(h1.toString());
        System.out.println(h3.toString());
    }
}
