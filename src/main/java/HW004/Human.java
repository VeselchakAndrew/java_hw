package HW004;

public class Human {
    private String name;
    private String surname;
    private int birthday;
    private int iq;
    private Pet pet;
    private Human mother;
    private Human father;
    private String[][] schedule;

    public Human(String name, String surname, int birthday) {
        this.name = name;
        this.surname = surname;
        this.birthday = birthday;
    }

    public Human(String name, String surname, int birthday, Human mother, Human father) {
        this.name = name;
        this.surname = surname;
        this.birthday = birthday;
        this.mother = mother;
        this.father = father;
    }

    public Human(String name, String surname, int birthday, int iq, Pet pet, Human mother, Human father, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.birthday = birthday;
        this.iq = iq;
        this.pet = pet;
        this.mother = mother;
        this.father = father;
        this.schedule = schedule;
    }

    public Human() {
    }

    ;

    public String getName() {
        return this.name;
    }

    public void greetPet() {
        System.out.printf("Привет, %s", this.pet.getNickname());
    }

    public void describePet() {

        String tricky;
        if (this.pet.getTrickLevel() > 50) {
            tricky = "очень хитрый";
        } else {
            tricky = "почти не хитрый";
        }

        System.out.printf("У меня есть %s\n, ему %d лет,\n он %s", this.pet.getSpecies(), this.pet.getAge(), tricky);
    }
//    Human{name='Michael', surname='Karleone', year=1977, iq=90,
//    mother=Jane Karleone, father=Vito Karleone,
//    pet=dog{nickname='Rock', age=5, trickLevel=75, habits=[eat, drink, sleep]}}

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("");
        String className = this.getClass().getSimpleName();
        sb
                .append(className)
                .append("{name = ").append(this.name)
                .append(", surname = ").append(this.surname)
                .append(", year = ").append(this.birthday)
                .append(", IQ = ").append(this.iq)
                .append(", mother = ").append(this.mother.name)
                .append(", father = ").append(this.father.name)
                .append(", pet = ").append(this.pet.toString());
        return sb.toString();
    }
}


