package HW001;

import java.util.Arrays;
import java.util.Scanner;

public class HW001 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int numberToGusess = (int) (Math.random() * 100);

        System.out.println("*******************\nLet the game begin!\n*******************\n");
        System.out.println("Enter your name: ");
        String userName = scanner.nextLine();
        int attempts = 0;
        int[] attemptsNumber = new int[20];
        boolean guess = false;

        while (!guess) {

            System.out.printf("Attempt number %d. Enter number:\n", attempts + 1);
            String userInput = scanner.nextLine();

            try {
                int number = Integer.parseInt(userInput);
                attemptsNumber[attempts] = number;
                attempts += 1;

                if (number > numberToGusess) {
                    System.out.println("Your number is too big. Please, try again.\n");
                } else if (number < numberToGusess) {
                    System.out.println("Your number is too small. Please, try again.\n");
                } else {
                    int[] finalAttemptsNumber = Arrays.copyOf(attemptsNumber, attempts);
                    System.out.printf("***************************\nCongratulations, %s!\nTotal attempts: %d\n***************************\n", userName, attempts);
                    System.out.println("Your numbers: " + Arrays.toString(finalAttemptsNumber));
                    guess = true;
                }
            } catch (NumberFormatException e) {
                System.err.print("Enter correct digit!\n");

            }
        }
    }
}


