package HW002;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class HW002 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter your name: ");
        String userName = scanner.nextLine();

        String[][] shootingArr = new String[6][6];

        for (int i = 0; i < shootingArr.length; i++) {
            for (int j = 0; j < shootingArr.length; j++) {
                shootingArr[i][j] = "-";
            }
        }

        int targetX = RandomNum(1, 5);
        int targetY = RandomNum(1, 5);
        System.out.println(targetY + " " + targetX);

        System.out.println("All set. Get ready to rumble!");

        boolean game = true;
        int shootY;
        int shootX;

        while (game) {

            shootY = InputData(1, 5, "horizontal", userName);
            shootX = InputData(1, 5, "vertical", userName);

            if (targetX == shootX && targetY == shootY) {
                shootingArr[shootX][shootY] = "X";
                System.out.printf("You have won, %s!\n", userName);
                PrintResult(shootingArr);
                game = false;

            } else {
                shootingArr[shootX][shootY] = "*";
                System.out.println("********************");
                System.out.println("You miss! Try again!");
                System.out.println("********************");
                PrintResult(shootingArr);
            }

        }
    }

    public static int RandomNum(int min, int max) {
        Random rand = new Random();
        return rand.nextInt((max - min) + 1) + min;
    }

    public static void PrintResult(String[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                if (i == 0) {
                    System.out.print(j + "\t");
                } else if (j == 0) {
                    System.out.print(i + "\t");
                } else {
                    System.out.print(arr[i][j] + "\t");
                }
            }
            System.out.println();
        }
    }

    public static int InputData(int min, int max, String direction, String userName) {
        Scanner scanner = new Scanner(System.in);
        int shoot;
        do {
            System.out.printf("Enter %s coord, %s (1 - 5): ", direction, userName);
            shoot = scanner.nextInt();
            if (shoot < 1 || shoot > 5) {
                System.out.println("Must be in 1 to 5 range");
            }
        } while (shoot < min || shoot > max);
        return shoot;
    }
}
