package HW003;

import java.util.Scanner;

public class HW003 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        String[][] schedule = new String[7][2];
        schedule[0][0] = "Sunday";
        schedule[0][1] = "do home work";
        schedule[1][0] = "Monday";
        schedule[1][1] = "go to courses; watch a film";
        schedule[2][0] = "Tuesday";
        schedule[2][1] = "go to walk";
        schedule[3][0] = "Wednesday";
        schedule[3][1] = "read the book";
        schedule[4][0] = "Thursday";
        schedule[4][1] = "meet with friends";
        schedule[5][0] = "Friday";
        schedule[5][1] = "drink beer with friends";
        schedule[6][0] = "Saturday";
        schedule[0][1] = "watch a study video; do home work";

        boolean isWorking = true;

        while (isWorking) {
            System.out.println("Enter day of week:");
            String userInput = scanner.nextLine().toLowerCase().trim();

            switch (userInput) {
                case ("sunday"): {
                    System.out.printf("Your tasks for %s: %s\n", firstUpperCase(userInput), schedule[0][1]);
                    break;
                }
                case ("monday"): {
                    System.out.printf("Your tasks for %s: %s\n", firstUpperCase(userInput), schedule[1][1]);
                    break;
                }
                case ("tuesday"): {
                    System.out.printf("Your tasks for %s: %s\n", firstUpperCase(userInput), schedule[2][1]);
                    break;
                }
                case ("wednesday"): {
                    System.out.printf("Your tasks for %s: %s\n", firstUpperCase(userInput), schedule[3][1]);
                    break;
                }
                case ("thursday"): {
                    System.out.printf("Your tasks for %s: %s\n", firstUpperCase(userInput), schedule[4][1]);
                    break;
                }
                case ("friday"): {
                    System.out.printf("Your tasks for %s: %s\n", firstUpperCase(userInput), schedule[5][1]);
                    break;
                }
                case ("saturday"): {
                    System.out.printf("Your tasks for %s: %s\n", firstUpperCase(userInput), schedule[6][1]);
                    break;
                }
                case ("exit"): {
                    isWorking = false;
                    break;
                }
                default: {
                    System.out.println("Sorry, I don't understand you, please try again.\nIf you want exit - type \"exit\"");
                }
            }
        }
    }

    public static String firstUpperCase(String word) {
        if (word == null || word.isEmpty()) return "";
        return word.substring(0, 1).toUpperCase() + word.substring(1);
    }


}
